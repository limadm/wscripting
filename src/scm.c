#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STANDALONE 0
#define _SCHEME_SOURCE 0
#include <scheme.h>

void scm_load_file(scheme *S, char *fn) {
	FILE *file = fopen(fn, "r");
	if (!file) {
		fprintf(stderr, "Could not open file: %s\n", fn);
	} else {
		scheme_load_named_file(S, file, fn);
		fclose(file);
	}
}

void scm_find_init(char *s, char *d) {
	int i = strlen(s);
	while (i > 1 && s[i-1] != '\\' && s[i-1] != '/') i--;
	sprintf(d, "%s", s);
	sprintf(&d[i], "init.scm");
}

int main(int argc, char *argv[]) {
	scheme *S = 0;
	int i, result;
	char scm_init[255];
	scm_find_init(argv[0], scm_init);
	S = scheme_init_new(S);
	if (!S) {
		fprintf(stderr, "Cannot malloc.");
		return 1;
	}
	scheme_set_input_port_file(S, stdin);
	scheme_set_output_port_file(S, stdout);
#if USE_DL
	scheme_define(S,S->global_env,mk_symbol(S,"load-extension"),mk_foreign_func(S, scm_load_ext));
#endif
	scm_load_file(S, scm_init);
	if (argc > 1) {
		for (i=1; i<argc; i++) scm_load_file(S, argv[i]);
	} else {
		scheme_load_named_file(S, stdin, "<stdin>");
	}
	result = S->retcode;
	scheme_deinit(S);
	return result;
}