#include <stdio.h>

#include <luajit.h>
#include <lauxlib.h>

char buffer[1024];

const char * read_chunk(lua_State *L, void *data, size_t *size) {
	*size = fread(buffer, 1, sizeof(buffer), stdin);
	return buffer;
}

int main(int argc, char *argv[]) {
	int i;
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	if (argc > 1) {
		for (i=1; i<argc; i++) {
			luaL_loadfile(L, argv[i]);
			lua_call(L, 0, 0);
		}
	} else {
		lua_load(L, read_chunk, 0, "<stdin>");
		lua_call(L, 0, 0);
	}
	return 0;
}