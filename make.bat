@echo off
setlocal
set arch=x86
if "%Platform%"=="X64" set suff=64
if "%Platform%"=="X64" set arch=x64

@echo Compiling (%arch%)...
cl /nologo /Fe: bin/lua%suff% src/lua.c /Iinclude /Os /MD /link lib/%arch%/lua51.lib      /nxcompat /release
cl /nologo /Fe: bin/scm%suff% src/scm.c /Iinclude /Os /MD /link lib/%arch%/tinyscheme.lib /nxcompat /release
del *.obj
del bin\*.exp
del bin\*.lib

@echo Done. Running tests:
bin\lua%suff% tests/test.lua
bin\scm%suff% tests/test.scm