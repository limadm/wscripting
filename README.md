# wscripting

Embedded scripting examples using:

- TinySCHEME (http://tinyscheme.sourceforge.net/)
- LuaJIT (http://luajit.org/) 

---

TinySCHEME is under a BSD-style license (c) 2000 Dimitrios Souflis.

LuaJIT is under MIT license (c) 2005-2015 Mike Pall.